//Codingan Soal No. 1
console.log("LOOPING PERTAMA");
var ulang1=2;
while(ulang1<=20){
    console.log(ulang1,"- I love coding");
    ulang1+=2;
}
console.log("LOOPING KEDUA");
var ulang2=20;
while(ulang2>=2){
    console.log(ulang2,"- I will become a mobile developer");
    ulang2-=2;
}

//Codingan Soal No.2
for(var ulang3=1; ulang3<=20; ulang3++){
    if(ulang3 %2 != 0 && ulang3 %3 == 0){
        console.log(ulang3,"- I Love Coding");
    }
    else if(ulang3 %2 == 0 ){
        console.log(ulang3,"- Berkualitas");
    }
    else{
        console.log(ulang3,"- Santai")
    }
}

//Codingan Soal No. 3
for(var ulang4=0; ulang4<=3; ulang4++){
        console.log("#".repeat(8))
}

//Codingan Soal No. 4
for(var ulang5=1; ulang5<=7; ulang5++){
        console.log("#".repeat(ulang5));
}

//Codingan Soal No. 5
for(var ulang6=1; ulang6<=8; ulang6++){
    if(ulang6 %2==0){
        console.log("# # # # ");
    }
    else{
        console.log(" # # # #");
    }
}
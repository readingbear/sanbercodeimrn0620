function range(startNum, finishNum) {
    if(startNum < finishNum || startNum > finishNum){
        var nilai=[];
        if(startNum < finishNum){
            while(startNum <= finishNum){
                nilai.push(startNum);
                startNum++;
            }
            return nilai;
        }
        else{
            while(startNum >= finishNum){
                nilai.push(startNum);
                startNum--;
            }
            return nilai;
        }
    }
    else if(!startNum || !finishNum){
        return "-1";
    }
    else{
        return startNum;
    }
} 
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

function rangeWithStep(startNum, finishNum,step) {
    if(startNum < finishNum || startNum > finishNum){
        var nilai=[];
        if(startNum < finishNum){
            while(startNum <= finishNum){
                nilai.push(startNum);
                startNum+=step;
            }
            return nilai;
        }
        else{
            while(startNum >= finishNum){
                nilai.push(startNum);
                startNum-=step;
            }
            return nilai;
        }
    }
    else if(!startNum || !finishNum){
        return -1;
    }
    else{
        return startNum;
    }
} 

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

function sum(startNum = 0, finishNum = 0, step=1){
    if(startNum < finishNum || startNum > finishNum){
        var nilai = rangeWithStep(startNum, finishNum,step);
        var hasil = nilai.reduce(function(a,b){return a+b},0);
        return hasil;
    }
    else{
        return startNum;
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

function dataHandling(data){
    for(var i=0 ; i < data.length; i++){
        console.log("Nomor ID : " + data[i][0]);
        console.log("Nama Lengkap : " + data[i][1]);
        console.log("TTL : " + data[i][2],data[i][3]);
        console.log("Hobi : " + data[i][4],"\n");
    }
} 
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input);

function balikKata(data){
    var balik="";
    for (var i = data.length-1; i>=0; i--){
        balik+=data[i];
    }
    return balik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

function dataHandling2(data){
    data.splice(1,2,"Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    data.splice(4,1,"Pria", "SMA Internasional Metro");
    console.log(data);
    var ttl = data[3].split("/");
    switch(ttl[1]){
        case "01" : {
            console.log("Januari");
            break;
        }
        case "02" : {
            console.log("Februari");
            break;
        }
        case "03" : {
            console.log("Maret");
            break;
        }
        case "04" : {
            console.log("April");
            break;
        }
        case "05" : {
            console.log("Mei");
            break;
        }
        case "06" : {
            console.log("Juni");
            break;
        }
        case "07" : {
            console.log("Juli");
            break;
        }
        case "08" : {
            console.log("Agustus");
            break;
        }
        case "09" : {
            console.log("September");
            break;
        }
        case "10" : {
            console.log("Oktober");
            break;
        }
        case "11" : {
            console.log("November");
            break;
        }
        case "12" : {
            console.log("Desember");
            break;
        }
    }
    var join = ttl.join("-");
    var desc = ttl.sort(function(a,b){return b-a});
    console.log(desc);
    console.log(join);
    var hsailslice = data[1].slice(0,14);
    console.log(hsailslice);
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
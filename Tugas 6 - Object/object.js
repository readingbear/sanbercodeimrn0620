function arrayToObject(arr) {
    // Code di sini
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var datacount = 1;
    var object = {};
    var maxdata = arr.length;
    if(!arr){
        console.log(" ");
    }
    else{
        while(datacount <= maxdata){
            object.firstname = arr[datacount-1][0];
            object.lastname = arr[datacount-1][1];
            object.gender = arr[datacount-1][2];
            if(!arr[datacount-1][3] || arr[datacount-1][3] > thisYear){
                object.age = "Invalid Birth Year";
            }
            else{
                object.age = thisYear - arr[datacount-1][3];
            }
            console.log(datacount+".",object.firstname,object.lastname,":",object);
            datacount+=1;
        }
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var object = {};
    var listbarang = ["Sepatu Stacattu","Baju Zoro","Baju H&N","Sweater Uniklooh","Casing Handphone"]
    var listharga = [1500000,500000,250000,175000,50000];
    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if(money < 50000){
        return "Mohon maaf, uang tidak cukup"
    }
    else{
        var total = 0;
        var spendMoney = money;
        object.memberId = memberId;
        object.money = money;
        object.listPurcahsed = "";
        for(var i = 0 ; i<=4 ;i++){
            if(spendMoney >= listharga[i]){
                object.listPurcahsed += listbarang [i]+", ";
                spendMoney -= listharga [i];
                total += listharga [i];
            }
        }
        object.changeMoney = money - total; 
        return object;
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08',2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742',170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('',2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53',15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var objek= [{ penumpang: '', naikDari: '', tujuan: '', bayar: 0 },
                  { penumpang: '', naikDari: '', tujuan: '', bayar: 0}];
    if(arrPenumpang == 0){
      return "[]";
    }
    else{
      var i = 0;
      while (i <= 1){
          var awal=0;
          var akhir=0;
          var bayar=0 ;
          objek[i].penumpang = arrPenumpang[i][0];
          objek[i].naikDari = arrPenumpang[i][1];
          objek[i].tujuan = arrPenumpang[i][2];
          for (var j = 0; j <= 5; j++){
              if( rute[j] == arrPenumpang[i][1]){
                  awal = j+1;
              }
              else if( arrPenumpang[i][2] == rute[j] ){
                  akhir = j+1;
              }
          }
          bayar = 2000*(akhir-awal)
          objek[i].bayar = bayar ;
          i ++;
      }
      return objek;
    }
  }
   
  //TEST CASE
  console.log(naikAngkot([["Dimitri","B","F"],["Icha","A","B"]]))
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]